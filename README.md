Contains configuration files for:

    - i3(lenovo spec)
    
    - bashrc
    
    - vimrc
    
    - tilix
    
    - powerline
    
    - vscode